package com.example.multitenantapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultitenantAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultitenantAppApplication.class, args);
    }

}
